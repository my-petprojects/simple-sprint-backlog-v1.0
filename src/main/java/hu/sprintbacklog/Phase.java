package hu.sprintbacklog;

public enum Phase {
    FORECAST,
    TODO,
    INPROGRESS,
    DONE
}
