
package hu.sprintbacklog;


public enum Priority {
    LOW,
    MEDIUM,
    HIGH,
    TOP
}
