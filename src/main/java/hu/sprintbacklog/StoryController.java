package hu.sprintbacklog;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StoryController {

    @Autowired
    private StoryService service;

    @RequestMapping("/")
    public String indexDisplay(Model model) {
        List<Story> storyList = service.listAll();
        model.addAttribute("storyList", storyList);
        return "index";
    }

    @RequestMapping("/new")
    public String newStoryDisplay(Model model) {
        Story story = new Story();
        model.addAttribute("story", story);
        return "newstory";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveStory(@ModelAttribute("story") Story story, BindingResult result) {

        if (result.hasErrors()) {
            return "newstory";
        }
        try {
            service.save(story);
        } catch (Throwable ex) {
            ex.getMessage();
            return "errorpage";
        }
        return "redirect:/";

    }

    @RequestMapping("/edit/{id}")
    public ModelAndView editStoryDisplay(@PathVariable(name = "id") int id) {
        ModelAndView modelAndView = new ModelAndView("editstory");
        Story story = service.get(id);
        modelAndView.addObject("story", story);
        return modelAndView;
    }

    @RequestMapping("/delete/{id}")
    public String deleteProduct(@PathVariable(name = "id") int id) {
        service.delete(id);
        return "redirect:/";
    }
}
