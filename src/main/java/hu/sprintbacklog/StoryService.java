package hu.sprintbacklog;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class StoryService {

	@Autowired
	private StoryRepository repository;
	
	public List<Story> listAll() {
		return repository.findAll();
	}
	
	public void save(Story product) {
		repository.save(product);
	}
	
	public Story get(long id) {
		return repository.findById(id).get();
	}
	
	public void delete(long id) {
		repository.deleteById(id);
	}
}
