Simple SpringBoot - MySQL app to organize and keep track of your weekly tasks.
You can find the local database name, Spring settings in application.properties.
After running the application navigate to http://localhost:8081 on your browser.